//
//  Item.swift
//  FunBoxTestTask
//
//  Created by vadim on 13/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class Item: NSObject, NSCoding, Codable {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(count, forKey: "count")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as! String
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.price = aDecoder.decodeDouble(forKey: "price")
        self.count = aDecoder.decodeInteger(forKey: "count")
    }
    
    let id: String
    var name:String
    var price:Double
    var count:Int
    
    init(name: String, price: Double, count: Int) {
        self.id = UUID().uuidString
        self.name = name
        self.price = price
        self.count = count
    }
}
