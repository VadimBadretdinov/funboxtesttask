//
//  BackEndViewController.swift
//  FunBoxTestTask
//
//  Created by vadim on 13/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class SFTableViewController: UIViewController {
    
    fileprivate let itemStore = ItemStore.shared
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
}

extension SFTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemStore.getItemStoreItems().filter {$0.count > 0}.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        let items = self.itemStore.getItemStoreItems().filter {$0.count > 0}
        cell.textLabel?.text = items[indexPath.row].name
        cell.detailTextLabel?.text = String("\(items[indexPath.row].count) шт.")
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SFDetailViewController,let indexPath = self.tableView.indexPathForSelectedRow {
            let items = self.itemStore.getItemStoreItems().filter {$0.count > 0}
            vc.item = items[indexPath.row]
        }
    }
}
