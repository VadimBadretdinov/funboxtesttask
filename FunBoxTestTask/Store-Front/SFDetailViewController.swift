//
//  ViewController.swift
//  FunBoxTestTask
//
//  Created by vadim on 13/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class SFDetailViewController: UIViewController {
    
    fileprivate let itemStore = ItemStore.shared
    
    var item: Item?
    
    @IBOutlet weak var deviceName: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var countTextField: UITextField!
    @IBAction func backButtonAction(_ sender: Any) {
        if let currentItem = item {
            self.itemStore.updateItem(updatedItem: currentItem)
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buyBtnAction(_ sender: Any) {
        if let text = self.countTextField.text, let count = Int(text), let currentItem = self.item {
            if count > 0 {
                self.countTextField.text = String(count - 1)
                self.itemStore.buyItem(item: currentItem)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let itemToSet = self.item {
            self.setItemValues(item: itemToSet)
        }
    }
    
    func setItemValues(item: Item) {
        self.deviceName.text = item.name
        self.priceTextField.text = String(item.price)
        self.countTextField.text = String(item.count)
    }
}

