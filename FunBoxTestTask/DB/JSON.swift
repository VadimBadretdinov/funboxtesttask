//
//  JSON.swift
//  FunBoxTestTask
//
//  Created by vadim on 17/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class JSON: DBprotocol {
    func load() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("json.txt")
        do {
            let jsonString = try String(contentsOf: fileUrl, encoding: .utf8)
            let decoder = JSONDecoder()
            let data = jsonString.data(using: .utf8)
            let jsonData = try decoder.decode([Item].self, from: data!)
            ItemStore.shared.loadItemsFromDB(items: jsonData)
        } catch {
            print("JSON loading error: \(error)")
        }
        
    }
    
    func save() {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("json.txt")
        do {
            let encoder = JSONEncoder()
            let jsonData = try encoder.encode(ItemStore.shared.getItemStoreItems())
            let jsonString = String(data: jsonData, encoding: .utf8)
            try jsonString?.write(to: fileUrl, atomically: true, encoding: .utf8)
        } catch {
            print("JSON saving error: \(error)")
        }
    }
}
