//
//  DBAdapter.swift
//  FunBoxTestTask
//
//  Created by vadim on 17/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

protocol DBprotocol {
    func load()
    func save()
}
