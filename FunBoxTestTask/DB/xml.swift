//
//  xml.swift
//  FunBoxTestTask
//
//  Created by vadim on 21/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import XMLCoder

class xml: DBprotocol {
    
    func load() {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("xml.txt")

        do {
            let xmlString = try String(contentsOf: fileUrl, encoding: .utf8)
            let xmlData = try Data(contentsOf: fileUrl)
            let decoder = XMLDecoder()
            if let data = xmlString.data(using: .utf8) {
                let xmlData = try decoder.decode([Item].self, from: xmlData)
                ItemStore.shared.loadItemsFromDB(items: xmlData)
            }
        } catch {
            print("XML loading error: \(error)")
        }
    }
    
    func save() {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("xml.txt")
        do {
            let encoder = XMLEncoder()
            let xmlData = try encoder.encode(ItemStore.shared.getItemStoreItems(), withRootKey: "item")
            let xmlString = String(data: xmlData, encoding: .utf8)
            try xmlString?.write(to: fileUrl, atomically: true, encoding: .utf8)
        } catch {
            print("XML saving error: \(error)")
        }
    }
}
