//
//  Binary.swift
//  FunBoxTestTask
//
//  Created by vadim on 22/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class Binary:DBprotocol {
    func load() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("binary.txt")
        do {
            let data = try Data(contentsOf: fileUrl)
            let unarchivedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! [Item]
            ItemStore.shared.loadItemsFromDB(items: unarchivedData)
        } catch {
            print("NSkeyedArchiver loading error: \(error)")
        }
    }
    
    func save() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("binary.txt")
        do {
            let archivedData = try NSKeyedArchiver.archivedData(withRootObject: ItemStore.shared.getItemStoreItems(), requiringSecureCoding: false)
            try archivedData.write(to: fileUrl)
        } catch {
            print("NSKeyedArchiver saving error: \(error)")
        }
    }
}
