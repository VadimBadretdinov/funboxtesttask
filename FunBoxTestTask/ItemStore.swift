//
//  ItemStore.swift
//  FunBoxTestTask
//
//  Created by vadim on 13/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import CSV

class ItemStore {
    fileprivate var items: [Item] = []
    
    static let shared = ItemStore()
    
    init() {
        self.parseCSV()
    }
    
    func buyItem(item: Item) {
        let queue = DispatchQueue(label: "buyItem", qos: .background)
        queue.async {
            sleep(3)
            item.count -= 1
        }
    }
    
    func addItem(item: Item) {
        ItemStore.shared.items.append(item)
    }
    
    func getItemsCount() -> Int {
        return ItemStore.shared.items.count
    }
    
    func getItemStoreItems() -> [Item] {
        return ItemStore.shared.items
    }
    
    func updateItem(updatedItem: Item) {
        let queue = DispatchQueue(label: "buyItem", qos: .background)
        queue.async {
            sleep(10)
            if let itemToUpdate = ItemStore.shared.items.filter({$0.id == updatedItem.id}).first {
                itemToUpdate.name = updatedItem.name
                itemToUpdate.price = updatedItem.price
                itemToUpdate.count = updatedItem.count
            }
        }
    }
    
    func deleteItem(itemToDelete: Item) {
        var index = 0
        for item in self.items {
            if item.id == itemToDelete.id {
                self.items.remove(at: index)
            }
            index += 1
        }
    }
    
    func loadItemsFromDB (items: [Item]) {
        self.items = items
    }
    
    fileprivate func parseCSV() {
        guard let path = Bundle.main.path(forResource: "data", ofType: "csv"), let stream = InputStream(fileAtPath: path), let csv = try? CSVReader(stream: stream, hasHeaderRow: false, trimFields: true, delimiter: ",", whitespaces: CharacterSet.whitespaces) else { return }
        while let row = csv.next() {
            if let doublePrice = Double(row[1]), let intCount = Int(row[2]) {
                self.items.append(Item(name: row[0], price: doublePrice, count: intCount))
            }
        }
    }
}

