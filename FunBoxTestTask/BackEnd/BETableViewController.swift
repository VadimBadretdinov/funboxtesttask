//
//  BackEndViewController.swift
//  FunBoxTestTask
//
//  Created by vadim on 13/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class BETableViewController: UIViewController {
    
    fileprivate let itemStore = ItemStore.shared
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
}

extension BETableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemStore.getItemsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        let items = self.itemStore.getItemStoreItems()
        cell.textLabel?.text = items[indexPath.row].name
        cell.detailTextLabel?.text = String("\(items[indexPath.row].count) шт.")
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? BEDetailViewController, let indexPath = self.tableView.indexPathForSelectedRow {
            let items = self.itemStore.getItemStoreItems()
            vc.item = items[indexPath.row]
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let items = self.itemStore.getItemStoreItems()
            self.itemStore.deleteItem(itemToDelete: items[indexPath.row])
            self.tableView.reloadData()
        }
    }
}
