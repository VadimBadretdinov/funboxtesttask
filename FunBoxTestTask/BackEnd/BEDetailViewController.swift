//
//  ViewController.swift
//  FunBoxTestTask
//
//  Created by vadim on 13/06/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class BEDetailViewController: UIViewController {
    
    fileprivate let itemStore = ItemStore.shared
    
    var item: Item?
    
    @IBOutlet weak var deviceName: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var countTextField: UITextField!
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if let name = self.deviceName.text, let price = self.priceTextField.text, let count = self.countTextField.text, let priceValue = Double(price), let countValue = Int(count){

            if let updatedItem = item {
                updatedItem.name = name
                updatedItem.price = Double(priceValue)
                updatedItem.count = Int(countValue)
                self.itemStore.updateItem(updatedItem: updatedItem)
            } else {
                self.itemStore.addItem(item: Item(name: name, price: priceValue, count: countValue))
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let itemToSet = item {
            self.setItemValues(item: itemToSet)
        }
        
        self.addGestureRecognizer()
    }
    
    func setItemValues(item: Item) {
        self.deviceName.text = item.name
        self.priceTextField.text = String(item.price)
        self.countTextField.text = String(item.count)
    }
}

extension BEDetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension UIViewController {
    func addGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
